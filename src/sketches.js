AFRAME.registerSystem('sketches', {
  init() {
    let sketchList = []
    for (let fileName of require.context('./sketches/', true, /.*png$/).keys()) {
      fileName = fileName.slice("./".length, - ".png".length)
      sketchList.push(require(`./sketches/${fileName}.png`))
    }
    this.sketchList = sketchList
  },
  async spawnImage() {
    let src = this.sketchList[Math.floor(Math.random() * this.sketchList.length)]
    let image = new Image()
    image.decoding = 'sync'
    image.loading = 'eager'
    image.src = src
    await image.decode()
    let el = document.createElement('a-plane')

    el.setAttribute('material', {shader: 'standard', src: image, npot: true})
    el.setAttribute('frame', '')
    el.setAttribute('width', 1)
    el.setAttribute('height', image.height / image.width)
    console.log("image", image.width, image.height)
    this.el.sceneEl.append(el)
    VARTISTE.Util.whenLoaded(el, () => {
      VARTISTE.Util.positionObject3DAtTarget(el.object3D, this.el.sceneEl.querySelector('#sketch-position').object3D)
      el.object3D.position.x += Math.random() * 0.2 - 0.1
      el.object3D.position.y += Math.random() * 0.2 - 0.1
      el.object3D.position.z += Math.random() * 0.2 - 0.1

      el.object3D.rotation.y += Math.random() * 0.1 - 0.05
    })
  }
})
