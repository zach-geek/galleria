const WebFont = require('webfontloader');

AFRAME.registerSystem('web-font-loader', {
  init() {
    this.loaded = {}
  },
  load(fontName) {
    if (this.loaded[fontName]) return Promise.resolve();

    return new Promise((r, e) => {
      WebFont.load({google: {families: [fontName]},
        fontactive: (f) => {
          console.log("Activated font", f, fontName, f === fontName)
          if (f === fontName) {
            this.el.emit('fontloaded', f)
            this.loaded[fontName] = true
            r()
          }
        },
      })
    })
  }
})

AFRAME.registerComponent('plaque', {
  schema: {
    width: {default: 4 / 3},
    height: {default: 3 / 3},
    margin: {default: 100},
    font: {default: 'Sansita Swashed'},
    fontSize: {default: 64},
    text: {default: ""},
    partial: {type: 'string', default: null},
    resolution: {default: 1024 * 3}
  },
  init() {
    this.el.setAttribute('geometry', 'primitive: plane')

    this.canvas = document.createElement('canvas')
    this.canvas.width = 3
    this.canvas.height = 3

    this.el.setAttribute('material', {shader: 'standard',
      src: '#asset-metal-color',
      roughnessMap: '#asset-metal-roughness',
      metalness: 1.0,
      transparent: false, npot: true, normalMap: this.canvas})
  },
  update(oldData) {
    this.el.setAttribute('geometry', 'width', this.data.width)
    this.el.setAttribute('geometry', 'height', this.data.height)
    this.canvas.width = Math.round(this.data.resolution)
    this.canvas.height = Math.round(this.data.resolution * this.data.height / this.data.width)

    if (this.data.partial) {
      this.data.text = require(`!!raw-loader!./partials/${this.data.partial}.txt`).default
    }

    this.el.sceneEl.systems['web-font-loader'].load(this.data.font).then(() => {
      console.log("Setting plaque text")
      let ctx = this.canvas.getContext('2d')
      ctx.fillStyle = "#333"
      ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)
      ctx.fillStyle = "#000"
      ctx.font = `${this.data.fontSize}px ${this.data.font}`;
      let lines = this.data.text.split("\n")
      for (i = 0; i < lines.length; ++i)
      {
        ctx.fillText(lines[i], this.data.margin, this.data.margin + this.data.fontSize * (i + 1))
      }
      VARTISTE.MaterialTransformations.bumpCanvasToNormalCanvas(ctx.canvas, {normalCanvas: this.canvas})
      this.el.getObject3D('mesh').material.normalMap.needsUpdate = true
    })
  }
})
