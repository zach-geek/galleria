//require('aframe-vartiste-toolkit')

export function loadAsset(fileName, {placeholder = false} = {}) {
  let asset = fileName.slice("./".length)
  if (asset.startsWith(".")) return

  let elementType = 'a-asset-item'

  let assetSrc = placeholder ? require(`./assets-placeholder/${asset}`) : require(`./assets/${asset}`)

// if (assetSrc.startsWith("asset/") && /\.(png|jpg)/i.test(assetSrc))
  if ((assetSrc.startsWith("asset/") && /\.(png|jpg)$/i.test(assetSrc) )
    || /^data:image/.test(assetSrc))
  {
    assetSrc = `${assetSrc}`
    elementType = 'img'
  }
  else if (asset.endsWith(".wav"))
  {
    elementType = 'audio'
  }
  //
  // if (assetSrc.startsWith("asset/") && window.VARTISTE_TOOLKIT_URL)
  // {
  //   assetSrc = `${VARTISTE_TOOLKIT_URL}/${assetSrc}`
  // }

  var element = document.createElement(elementType)

  element.setAttribute("src", assetSrc)
  element.id = `asset-${asset.split(".")[0]}`
  element.setAttribute('crossorigin',"anonymous")
  return element
}

export function loadAllAssets() {
  let loadedRealAssets = false
  try {
    for (let fileName of require.context('./assets/', true, /.*/).keys()) {
      let loadedRealAssets = true
      document.querySelector('a-assets').append(loadAsset(fileName))
    }
  }catch (e) {
    loadedRealAssets = false
  }

  if (!loadedRealAssets) {
    for (let fileName of require.context('./assets-placeholder/', true, /.*/).keys()) {
      document.querySelector('a-assets').append(loadAsset(fileName, {placeholder: true}))
    }
  }
}

document.write(require('./galleria.html.slm'))
loadAllAssets()


require('./fixes.js')
require('./sketches')
require('./cart')
require('./plaque.js')
require('./artthings.js')

require('!!file-loader?name=preview.jpg!./preview.jpg')
