AFRAME.registerComponent('eyes-look-at-camera', {
  init() {
    this.tick = AFRAME.utils.throttleTick(this.tick, 30, this)
    VARTISTE.Pool.init(this)
    this.center = new THREE.Vector3()
    this.up = new THREE.Vector3(0, 0, -1)
  },
  tick(t, dt) {

    if (!this.el.getObject3D('mesh')) return;
    if (!this.leftEye) {
      this.leftEye = this.el.getObject3D('mesh').getObjectByName('LeftEye')
      // this.leftEye.up.set(0, 0, 1)
      this.leftEye.up.applyQuaternion(this.leftEye.quaternion)
      this.leftEye.add(new THREE.AxesHelper)
    }
    if (!this.rightEye) {
      this.rightEye = this.el.getObject3D('mesh').getObjectByName('RightEye')
      this.rightEye.up.set(0, 0, 1)
      // this.rightEye.up.applyQuaternion(this.rightEye.quaternion)
      this.rightEye.add(new THREE.AxesHelper)
    }
    if (!this.leftEye || !this.rightEye) return

    // return;

    let pos = this.pool('pos', THREE.Vector3)
    VARTISTE.Util.cameraObject3D().getWorldPosition(pos)
    // this.leftEye.lookAt(pos)
    // this.rightEye.lookAt(pos)

    let mat = this.pool('mat', THREE.Matrix4)

    this.leftEye.worldToLocal(pos)
    mat.copy(this.leftEye.matrix)
    mat.lookAt(this.center, pos, this.leftEye.up)
    mat.extractRotation(mat)
    this.leftEye.quaternion.setFromRotationMatrix(mat)

    // VARTISTE.Util.applyMatrix(this.leftEye.matrix, this.leftEye)
    // this.rightEye.matrix.lookAt(pos, this.center, this.up)
    // VARTISTE.Util.applyMatrix(this.rightEye.matrix, this.rightEye)
  }
})
